import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RegisterComponent } from './register/register.component';
import { RegisterService } from './shared/register.service';
import { ValidatePhoneDirective } from './validate-phone.directive';
import { AcceptedComponent } from './accepted/accepted.component';
import { RouterModule } from '@angular/router';

const ROUTES = [
  {path: '', component: RegisterComponent},
  {path: 'accepted', component: AcceptedComponent},
]

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    ValidatePhoneDirective,
    AcceptedComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(ROUTES),
  ],
  providers: [RegisterService],
  bootstrap: [AppComponent]
})
export class AppModule { }
