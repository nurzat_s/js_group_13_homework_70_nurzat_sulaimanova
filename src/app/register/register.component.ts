import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { RegisterService } from '../shared/register.service';
import { phoneValidator } from '../validate-phone.directive';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { Register } from '../shared/register.model';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, OnDestroy{
  @ViewChild('f') userForm!: NgForm;
  registerForm!: FormGroup;

  isUploading = false;
  formUploadingSubscription!: Subscription;

  constructor(private registerService: RegisterService, private router: Router) { }

  ngOnInit(): void {
    this.registerForm = new FormGroup({
      firstName: new FormControl('', Validators.required),
      secondName: new FormControl('', Validators.required),
      patronymicName: new FormControl('', Validators.required),
      phone: new FormControl('', [Validators.required, phoneValidator]),
      job: new FormControl('', Validators.required),
      gender: new FormControl('', Validators.required),
      size: new FormControl('', Validators.required),
      comment: new FormControl('', [Validators.required, Validators.maxLength(300)]),
      skills: new FormArray([])
    });

    this.formUploadingSubscription = this.registerService.registerUploading.subscribe((isUploading: boolean) => {
      this.isUploading = isUploading;
    });
  }


  onSubmit() {
    console.log(this.registerForm);
  }

  fieldHasError(fieldName: string, errorType: string) {
    const field = this.registerForm.get(fieldName);
    return Boolean (field && field.touched && field.errors?.[errorType]);
  }

  addSkill() {
    const skills = <FormArray>this.registerForm.get('skills');
    const skillsGroup = new FormGroup({
      skill: new FormControl('', Validators.required),
      level: new FormControl('')
    })
    skills.push(skillsGroup);
  }

  getSkillControls() {
    const skills = <FormArray>this.registerForm.get('skills');
    return skills.controls;
  }

  saveForm() {
    const id = Math.random().toString();

    const register = new Register(
      id,
      this.userForm.value.firstName,
      this.userForm.value.secondName,
      this.userForm.value.patronymicName,
      this.userForm.value.phone,
      this.userForm.value.job,
      this.userForm.value.gender,
      this.userForm.value.size,
      this.userForm.value.comment,
      this.userForm.value.skills
    );

    const next = () => {
      void this.router.navigate(['/accepted']);
    };

    this.registerService.addForm(register).subscribe(next);
  }

  ngOnDestroy() {
    this.formUploadingSubscription.unsubscribe();
  }
}
