import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Register } from './register.model';

@Injectable()

export class RegisterService {
  registerUploading = new Subject<boolean>();

  constructor(private http: HttpClient) {}


  addForm(register: Register) {
    const body = {
      firstName: register.firstName,
      secondName: register.secondName,
      patronymicName: register.patronymicName,
      phone: register.phone,
      job: register.job,
      gender: register.gender,
      size: register.size,
      comments: register.comment,
      skills: register.skills
    };

    this.registerUploading.next(true);

    return this.http.post('https://plovo-e531e-default-rtdb.firebaseio.com/registration.json', body).pipe(
      tap(() => {
        this.registerUploading.next(false);
      }, () => {
        this.registerUploading.next(false);
      })
    );
  }
}

