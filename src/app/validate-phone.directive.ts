import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';
import { Directive } from '@angular/core';

export const phoneValidator =  (control: AbstractControl): ValidationErrors | null => {
  const hasNumber = /^[+][0-9]{12}$/.test(control.value);

  if(hasNumber) {
    return null;
  }
  return {phoneNumber: true};
}

@Directive({
  selector: '[appPhone]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: ValidatePhoneDirective,
    multi: true
  }]
})

export class ValidatePhoneDirective implements Validator {
  validate(control: AbstractControl): ValidationErrors | null {
    return phoneValidator(control);
  }
}
